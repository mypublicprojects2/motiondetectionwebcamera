﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging;
using System.Security;
using AForge.Vision.Motion;

namespace MotionDetectionWebCamera
{
    public partial class CaptureDevice : Form
    {
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice Camera;
        public CaptureDevice()
        {
            InitializeComponent();
            //prikaz mogućih uređaja
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                cbDevices.Items.Add(VideoCaptureDevice.Name);
            }
            cbDevices.SelectedIndex = 0;
        }

        private void CaptureDevice_Load(object sender, EventArgs e)
        {
           
        }
        //button za pokretanje rada kamere
        private void btnStart_Click(object sender, EventArgs e)
        {
            Camera = new VideoCaptureDevice(VideoCaptureDevices[cbDevices.SelectedIndex].MonikerString);
            MovmentDetectionMonitor movmentDetectionMonitor = new MovmentDetectionMonitor(Camera);
            movmentDetectionMonitor.Show();
            this.Hide();
        }
        //button za odlazak na formu sa povjesti detekcije kretanja
        private void btnDataBase_Click(object sender, EventArgs e)
        {
            MovmentHistory movmentHistory = new MovmentHistory();
            movmentHistory.Show();
            this.Hide();
        }

        private void CaptureDevice_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
