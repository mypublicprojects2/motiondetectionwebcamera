﻿
namespace MotionDetectionWebCamera
{
    partial class CaptureDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbUvodniTekst = new System.Windows.Forms.Label();
            this.cbDevices = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnDataBase = new System.Windows.Forms.Button();
            this.pbVUBlogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbVUBlogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lbUvodniTekst
            // 
            this.lbUvodniTekst.AutoSize = true;
            this.lbUvodniTekst.Location = new System.Drawing.Point(329, 53);
            this.lbUvodniTekst.Name = "lbUvodniTekst";
            this.lbUvodniTekst.Size = new System.Drawing.Size(200, 13);
            this.lbUvodniTekst.TabIndex = 1;
            this.lbUvodniTekst.Text = "Odaberite uređaj za detektiranje pokreta:";
            // 
            // cbDevices
            // 
            this.cbDevices.FormattingEnabled = true;
            this.cbDevices.Location = new System.Drawing.Point(247, 91);
            this.cbDevices.Name = "cbDevices";
            this.cbDevices.Size = new System.Drawing.Size(371, 21);
            this.cbDevices.TabIndex = 2;
            this.cbDevices.SelectedIndexChanged += new System.EventHandler(this.CaptureDevice_Load);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(262, 141);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(141, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Početak Detekcije";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnDataBase
            // 
            this.btnDataBase.Location = new System.Drawing.Point(462, 141);
            this.btnDataBase.Name = "btnDataBase";
            this.btnDataBase.Size = new System.Drawing.Size(141, 23);
            this.btnDataBase.TabIndex = 4;
            this.btnDataBase.Text = "Povijest Kretanja";
            this.btnDataBase.UseVisualStyleBackColor = true;
            this.btnDataBase.Click += new System.EventHandler(this.btnDataBase_Click);
            // 
            // pbVUBlogo
            // 
            this.pbVUBlogo.Image = global::MotionDetectionWebCamera.Properties.Resources.vub_logo;
            this.pbVUBlogo.Location = new System.Drawing.Point(12, 12);
            this.pbVUBlogo.Name = "pbVUBlogo";
            this.pbVUBlogo.Size = new System.Drawing.Size(195, 192);
            this.pbVUBlogo.TabIndex = 0;
            this.pbVUBlogo.TabStop = false;
            // 
            // CaptureDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 220);
            this.Controls.Add(this.btnDataBase);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.cbDevices);
            this.Controls.Add(this.lbUvodniTekst);
            this.Controls.Add(this.pbVUBlogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CaptureDevice";
            this.Text = "CaptureDevice";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CaptureDevice_FormClosed);
            this.Load += new System.EventHandler(this.CaptureDevice_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbVUBlogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbVUBlogo;
        private System.Windows.Forms.Label lbUvodniTekst;
        private System.Windows.Forms.ComboBox cbDevices;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnDataBase;
    }
}