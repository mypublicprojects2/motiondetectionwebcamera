using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace MotionDetectionWebCamera
{
    public partial class MotionsPicEntities : DbContext
    {
        public MotionsPicEntities()
            : base("name=MotionsPicEntities1")
        {
        }

        public virtual DbSet<MotionsPic> MotionsPic { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MotionsPic>()
                .Property(e => e.RefPicture)
                .IsUnicode(false);
        }
    }
}
