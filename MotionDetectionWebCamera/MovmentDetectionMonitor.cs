﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging;
using System.Security;
using AForge.Vision.Motion;

namespace MotionDetectionWebCamera
{
    public partial class MovmentDetectionMonitor : Form
    {
       
        private VideoCaptureDevice Camera;
        private MotionDetector Detector;
        float vrijednost;
        float tmp;
        Bitmap current;
        public MovmentDetectionMonitor(VideoCaptureDevice Camera)
        {
            this.Camera = Camera;
            InitializeComponent();
        }
        
        private void MovmentDetectionMonitor_Load(object sender, EventArgs e)
        {
            Detector = new MotionDetector(new TwoFramesDifferenceDetector(), new MotionBorderHighlighting());
            vrijednost = 0;
            tmp = 0;
            videoSourcePlayer1.VideoSource = Camera;
            
        }
        //Detecting Motions
        private void videoSourcePlayer1_NewFrame(object sender, ref Bitmap image)
        {
            vrijednost = Detector.ProcessFrame(image);
            
        }
        //Saving Date,and reference to picture when vrijednost change because motion is detected
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (vrijednost != tmp)
            {
                DateTime localDate = DateTime.Now;
                //MotionsList.AddMotions(localDate);
                tmp = vrijednost;
                //geting current frame when motion detected and saveing in MyPhotos folder
                //Bitmap current = (Bitmap)videoSourcePlayer1.GetCurrentVideoFrame();
                string fileName= DateTimeNamePicture.GetName();
                System.Console.WriteLine(fileName);
                current.Save(fileName);
                current.Dispose();
                MotionsList.AddMotionsPic(localDate,fileName);
               
            }
        }
        //vraća ma formu za odabir kamere
        private void btnSwitch_Click(object sender, EventArgs e)
        {
            CaptureDevice captureDevice= new CaptureDevice();
            videoSourcePlayer1.SignalToStop();
            Camera.SignalToStop();
            captureDevice.Show();
            this.Hide();
        }
        //button for start and stop of Motion Detection and Camera.
        private void btnStop_Click(object sender, EventArgs e)
        {
            if (videoSourcePlayer1.IsRunning)
            {
                videoSourcePlayer1.SignalToStop();
                Camera.SignalToStop();
            }
            else
            {
                videoSourcePlayer1.Start();
                //making function for Camera frame
                Camera.NewFrame += Camera_NewFrame;
                Camera.Start();
            }
               
        }

        private void Camera_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            current = (Bitmap)eventArgs.Frame.Clone();
           
        }

        private void btnDataBase_Click(object sender, EventArgs e)
        {
            MovmentHistory movmentHistory = new MovmentHistory();
            videoSourcePlayer1.SignalToStop();
            Camera.SignalToStop();
            movmentHistory.Show();
            this.Hide();
        }

        private void videoSourcePlayer1_Click(object sender, EventArgs e)
        {

        }

        private void MovmentDetectionMonitor_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
