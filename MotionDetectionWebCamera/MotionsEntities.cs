using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace MotionDetectionWebCamera
{
    public partial class MotionsEntities : DbContext
    {
        public MotionsEntities()
            : base("name=MotionsEntities")
        {
        }

        public virtual DbSet<Motions> Motions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
