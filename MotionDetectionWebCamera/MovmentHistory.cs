﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MotionDetectionWebCamera
{
    public partial class MovmentHistory : Form
    {
        public MovmentHistory()
        {
            InitializeComponent();
        }

        private void btnCaptureDevice_Click(object sender, EventArgs e)
        {
            CaptureDevice captureDevice = new CaptureDevice();
            captureDevice.Show();
            this.Hide();
        }

        private void btnChose_Click(object sender, EventArgs e)
        {
             //Console.WriteLine(MotionsList.GetMotionsPic(lbMovments.SelectedItem.ToString()));
            //EXCEPTION
            try
            {
                pbDataBasePic.Image = Image.FromFile(@MotionsList.GetMotionsPic(lbMovments.SelectedItem.ToString()));
            }
            catch (Exception e2)
            {
                Console.WriteLine($"Generic Exception Handler: {e2}");
                MessageBox.Show("Nema slike u bazi!");
            }
        }

        private void MovmentHistory_Load(object sender, EventArgs e)
        {
            RefreshMotions();
        }
        private void RefreshMotions()
        {
            lbMovments.Items.Clear();
            var context = new MotionsPicEntities();
            var motion =
                from m in context.MotionsPic
                select m;
            foreach (MotionsPic m in motion)
            {
                lbMovments.Items.Add(m.Time);
            }
            context.Dispose();
        }

        private void MovmentHistory_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
