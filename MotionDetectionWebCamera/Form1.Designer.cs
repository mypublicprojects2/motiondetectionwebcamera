﻿
namespace MotionDetectionWebCamera
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbDeviceCapture = new System.Windows.Forms.ComboBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.lbMotions = new System.Windows.Forms.ListBox();
            this.vdCamera = new AForge.Controls.VideoSourcePlayer();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbValue = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // cbDeviceCapture
            // 
            this.cbDeviceCapture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDeviceCapture.FormattingEnabled = true;
            this.cbDeviceCapture.Location = new System.Drawing.Point(560, 25);
            this.cbDeviceCapture.Name = "cbDeviceCapture";
            this.cbDeviceCapture.Size = new System.Drawing.Size(213, 21);
            this.cbDeviceCapture.TabIndex = 1;
            this.cbDeviceCapture.SelectedIndexChanged += new System.EventHandler(this.cbDeviceCapture_SelectedIndexChanged);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(560, 397);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStop.Location = new System.Drawing.Point(642, 397);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop Recording";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // lbMotions
            // 
            this.lbMotions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMotions.FormattingEnabled = true;
            this.lbMotions.Location = new System.Drawing.Point(560, 72);
            this.lbMotions.Name = "lbMotions";
            this.lbMotions.Size = new System.Drawing.Size(213, 303);
            this.lbMotions.TabIndex = 4;
            this.lbMotions.SelectedIndexChanged += new System.EventHandler(this.lbMotions_SelectedIndexChanged);
            // 
            // vdCamera
            // 
            this.vdCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vdCamera.Location = new System.Drawing.Point(23, 25);
            this.vdCamera.Name = "vdCamera";
            this.vdCamera.Size = new System.Drawing.Size(480, 350);
            this.vdCamera.TabIndex = 5;
            this.vdCamera.Text = "Camera";
            this.vdCamera.VideoSource = null;
            this.vdCamera.NewFrame += new AForge.Controls.VideoSourcePlayer.NewFrameHandler(this.vdCamera_NewFrame);
            this.vdCamera.Click += new System.EventHandler(this.videoSourcePlayer1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbValue
            // 
            this.lbValue.AutoSize = true;
            this.lbValue.Location = new System.Drawing.Point(23, 397);
            this.lbValue.Name = "lbValue";
            this.lbValue.Size = new System.Drawing.Size(46, 13);
            this.lbValue.TabIndex = 6;
            this.lbValue.Text = "Value: 0";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbValue);
            this.Controls.Add(this.vdCamera);
            this.Controls.Add(this.lbMotions);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.cbDeviceCapture);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbDeviceCapture;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.ListBox lbMotions;
        private AForge.Controls.VideoSourcePlayer vdCamera;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbValue;
        private System.Windows.Forms.Timer timer2;
    }
}

