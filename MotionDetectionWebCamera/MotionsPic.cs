namespace MotionDetectionWebCamera
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MotionsPic")]
    public partial class MotionsPic
    {
        public int Id { get; set; }

        public DateTime Time { get; set; }

        [Required]
        [StringLength(4000)]
        public string RefPicture { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", Time);
        }

    }
}
