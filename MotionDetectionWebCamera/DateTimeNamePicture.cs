﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MotionDetectionWebCamera
{
    class DateTimeNamePicture
    {   
        static string SpecialFileName
        {
            get
            {
                // Part A: specify the format string.
                return string.Format("{0}{1}MotionDetectionPic-{2:yyyy-MM-dd_hh-mm-ss}.bmp",
                    // Part B: call GetFolderPath.
                    Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                    // Part C: specify separator char.
                    Path.DirectorySeparatorChar,
                    // Part D: get current time.
                    DateTime.Now);
            }
        }

        public static string GetName() {
            return SpecialFileName;
        }
    }
}
