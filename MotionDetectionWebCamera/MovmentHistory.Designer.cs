﻿
namespace MotionDetectionWebCamera
{
    partial class MovmentHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lb1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lbPictureFromBase = new System.Windows.Forms.Label();
            this.pbDataBasePic = new System.Windows.Forms.PictureBox();
            this.lbMovments = new System.Windows.Forms.ListBox();
            this.btnCaptureDevice = new System.Windows.Forms.Button();
            this.btnChose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbDataBasePic)).BeginInit();
            this.SuspendLayout();
            // 
            // lb1
            // 
            this.lb1.AutoSize = true;
            this.lb1.Location = new System.Drawing.Point(48, 13);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(133, 13);
            this.lb1.TabIndex = 0;
            this.lb1.Text = "Povijest detektiranih kretnji";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lbPictureFromBase
            // 
            this.lbPictureFromBase.AutoSize = true;
            this.lbPictureFromBase.Location = new System.Drawing.Point(428, 13);
            this.lbPictureFromBase.Name = "lbPictureFromBase";
            this.lbPictureFromBase.Size = new System.Drawing.Size(66, 13);
            this.lbPictureFromBase.TabIndex = 3;
            this.lbPictureFromBase.Text = "Slika iz baze";
            // 
            // pbDataBasePic
            // 
            this.pbDataBasePic.Location = new System.Drawing.Point(326, 44);
            this.pbDataBasePic.Name = "pbDataBasePic";
            this.pbDataBasePic.Size = new System.Drawing.Size(517, 483);
            this.pbDataBasePic.TabIndex = 4;
            this.pbDataBasePic.TabStop = false;
            // 
            // lbMovments
            // 
            this.lbMovments.FormattingEnabled = true;
            this.lbMovments.Location = new System.Drawing.Point(51, 44);
            this.lbMovments.Name = "lbMovments";
            this.lbMovments.Size = new System.Drawing.Size(234, 446);
            this.lbMovments.TabIndex = 5;
            // 
            // btnCaptureDevice
            // 
            this.btnCaptureDevice.Location = new System.Drawing.Point(26, 504);
            this.btnCaptureDevice.Name = "btnCaptureDevice";
            this.btnCaptureDevice.Size = new System.Drawing.Size(130, 23);
            this.btnCaptureDevice.TabIndex = 6;
            this.btnCaptureDevice.Text = "Izaberi Kameru";
            this.btnCaptureDevice.UseVisualStyleBackColor = true;
            this.btnCaptureDevice.Click += new System.EventHandler(this.btnCaptureDevice_Click);
            // 
            // btnChose
            // 
            this.btnChose.Location = new System.Drawing.Point(162, 504);
            this.btnChose.Name = "btnChose";
            this.btnChose.Size = new System.Drawing.Size(141, 23);
            this.btnChose.TabIndex = 7;
            this.btnChose.Text = "Odaberi";
            this.btnChose.UseVisualStyleBackColor = true;
            this.btnChose.Click += new System.EventHandler(this.btnChose_Click);
            // 
            // MovmentHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 560);
            this.Controls.Add(this.btnChose);
            this.Controls.Add(this.btnCaptureDevice);
            this.Controls.Add(this.lbMovments);
            this.Controls.Add(this.pbDataBasePic);
            this.Controls.Add(this.lbPictureFromBase);
            this.Controls.Add(this.lb1);
            this.Name = "MovmentHistory";
            this.Text = "MovmentHistory";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MovmentHistory_FormClosed);
            this.Load += new System.EventHandler(this.MovmentHistory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbDataBasePic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label lbPictureFromBase;
        private System.Windows.Forms.PictureBox pbDataBasePic;
        private System.Windows.Forms.ListBox lbMovments;
        private System.Windows.Forms.Button btnCaptureDevice;
        private System.Windows.Forms.Button btnChose;
    }
}