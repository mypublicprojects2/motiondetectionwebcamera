﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using AForge.Imaging;
using System.Security;
using AForge.Vision.Motion;


namespace MotionDetectionWebCamera
{
    public partial class Form1 : Form
    {
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice Camera;
        private MotionDetector Detector;
        float vrijednost;
        float tmp;


        public Form1()
        {
            InitializeComponent();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //konstruktor 
            Detector = new MotionDetector(new TwoFramesDifferenceDetector(), new MotionBorderHighlighting());
            vrijednost = 0;
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                cbDeviceCapture.Items.Add(VideoCaptureDevice.Name);
            }
            cbDeviceCapture.SelectedIndex = 0;
            tmp = 0;
            RefreshMotions();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Camera = new VideoCaptureDevice(VideoCaptureDevices[cbDeviceCapture.SelectedIndex].MonikerString);
            vdCamera.VideoSource = Camera;
            vdCamera.Start();
        
        }


        private void Form1_ForceClosing(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            vdCamera.SignalToStop();
        }

        private void lbMotions_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void videoSourcePlayer1_Click(object sender, EventArgs e)
        {

        }

        private void vdCamera_NewFrame(object sender, ref Bitmap image)
        {

            vrijednost = Detector.ProcessFrame(image);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
                lbValue.Text = "Value" + vrijednost.ToString();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

            if (vrijednost != tmp)
            {
                DateTime localDate = DateTime.Now;
                MotionsList.AddMotions(localDate);
                RefreshMotions();
                tmp = vrijednost; 
                MessageBox.Show("Kretnja detektirana!!!");

            }
           

        }
        private void RefreshMotions()
        {
            lbMotions.Items.Clear();
            var context = new MotionsEntities();
            var motion =
                from m in context.Motions
                select m;
            foreach (Motions m in motion)
            {
                lbMotions.Items.Add(m);
            }
            context.Dispose();
        }

        private void cbDeviceCapture_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
